-- CreateTable
CREATE TABLE "Language" (
    "name" TEXT NOT NULL,
    "abbrev" CHAR(2) NOT NULL,

    CONSTRAINT "Language_pkey" PRIMARY KEY ("abbrev")
);

-- CreateTable
CREATE TABLE "Word" (
    "id" SERIAL NOT NULL,
    "word" TEXT NOT NULL,
    "length" INTEGER NOT NULL,
    "lang" CHAR(2) NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "createdBy" CHAR(18) NOT NULL,
    "approved_at" TIMESTAMP(3),
    "approvedBy" CHAR(18),
    "meaning" TEXT,
    "reference" TEXT,

    CONSTRAINT "Word_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Word_word_key" ON "Word"("word");

-- AddForeignKey
ALTER TABLE "Word" ADD CONSTRAINT "Word_lang_fkey" FOREIGN KEY ("lang") REFERENCES "Language"("abbrev") ON DELETE RESTRICT ON UPDATE CASCADE;
