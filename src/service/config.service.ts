import { Intents } from 'discord.js';

class ConfigService {
  public developers: string[] = ['756757056941326397'];

  public intents: number[] = [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_MESSAGES,
    Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
    Intents.FLAGS.GUILD_EMOJIS_AND_STICKERS,
  ];
}

const instance = new ConfigService();
export default instance;
