import { prisma } from '@/app';
import { Word, Prisma } from '@prisma/client';

const ISO6391 = require('iso-639-1');
export const SUCCESS = 0;
export const PRISMA_CON_ERROR = 1;
export const WORD_EXISTS = 2;
export const INVALID_WORD = 3;
export const WORD_BANNED = 4;

class DatabaseService {
  cache: { word: Word, hits: number }[] = [];

  async registerEnglishWord(word: string, author: string): Promise<number> {
    return await this.registerWord(word, 'en', author);
  }

  async registerWord(word: string, lang: string, author: string): Promise<number> {
    let exists = await this.existsWord(word);
    if(word.split(' ').length !== 1) {
      return INVALID_WORD;
    }

    if(exists) {
      try {
        await prisma.word.update({
          where: { word },
          data: { uses: { increment: 1 } }
        });

        if(exists.disapprovalReason) {
          return WORD_BANNED;
        }

        return WORD_EXISTS;
      } catch(err) {
        console.error(err)
        return PRISMA_CON_ERROR;
      }
    }


    word = word.toLowerCase();
    try {
      await prisma.word.create({
        data: {
          word, length: word.length, createdBy: author,
          language: {
            connectOrCreate: {
              where: { abbrev: lang },
              create: {
                name: ISO6391.getName(lang) ?? 'Unknown',
                abbrev: lang
              }
            }
          }
        }
      });
      return SUCCESS;
    } catch(err) {
      console.error(err);
      return PRISMA_CON_ERROR;
    }
  }

  async approveWord(
    word: string, 
    definition: string, 
    reference: string,
    approver: string,
    lang?: string
  ): Promise<boolean> {
    let exists = await this.existsWord(word);

    if(!exists) {
      return false;
    }

    try {
      let data: Prisma.WordUpdateInput = {
        meaning: definition,
        reference: reference,
        approvedAt: new Date(),
        approvedBy: approver,
        disapprovalReason: null
      };
      if(lang) {
        Object.assign(data, { lang });
      }

      await prisma.word.update({ where: { word }, data });
      return true;
    } catch(err) {
      console.error(err);
      return false;
    }
  }

  async disapproveWord(word: string, disapprovedBy: string, reason: string) {
    let exists = await this.existsWord(word);
    if(!exists) {
      return false;
    }

    try {
      await prisma.word.update({ where: { word }, data: {
        approvedAt: new Date(),
        approvedBy: disapprovedBy,
        disapprovalReason: reason
      }});
      return true;
    } catch(err) {
      console.error(err);
      return false;
    }
  }

  async existsWord(word: string): Promise<Word | null> {
    for(let entry of this.cache) {
      if(entry.word.word === word) {
        entry.hits++;
        this.cache.sort((e1, e2) => {
          return e1.hits < e2.hits ? 1 : (
            e1.hits > e2.hits ? -1 : 0
          );
        });
        return entry.word;
      }
    }

    try {
      let found = await prisma.word.findFirst({ where: { word: word } });
      if(found) {
        this.cache = [...this.cache, { word: found, hits: 1 }];
      }
      return found;
    } catch(err) {
      console.error(err);
      return null;
    }
  }

  async getEnglishWords(length: number): Promise<Array<Word>> {
    return await this.getWords('en', length);
  }

  async getWords(lang: string, length: number): Promise<Array<Word>> {
    try {
      return await prisma.word.findMany({ 
        where: { 
          lang, 
          length,
          approvedBy: { not: null },
          disapprovalReason: { equals: null }
        } 
      });
    } catch(err) {
      console.error(err);
      return [];
    }
  }
}

const instance = new DatabaseService();
export default instance;
