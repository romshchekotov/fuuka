import * as fs from 'fs/promises';
import * as fsSync from 'fs';
import * as inquirer from 'inquirer';
import axios from 'axios';

const API_URL = 'https://api.dictionaryapi.dev/api/v2/entries/en';

function assemble(state: number[]) {
  return String.fromCharCode(... state.map(ch => ch + 97));
}

async function start() {
  let initCheck = fsSync.existsSync('res/crawler.json');
  if(!initCheck) {
    fsSync.writeFileSync('res/crawler.json', "[0, 0, 0, 0, 0]");
  }

  let state = JSON.parse(await fs.readFile('res/crawler.json', 'utf-8'));
  let pointer = 4;

  proc:
  while(state[0] < 26) {
    while(state[pointer] < 26) {
      while(pointer < 4) {
        pointer++;
      }
      let word = assemble(state);
      let answer = await inquirer.prompt([{
        type: 'list',
        name: 'choice',
        message: word,
        choices: ['valid', 'invalid', 'quit']
      }]);
      if(answer.choice === 'quit') {
        break proc;
      } else if(answer.choice === 'valid') {
        try {
          let res = await axios.get(`${API_URL}/${word}`);
          if(res.status !== 200 || res.data.length === 0) {
            console.log(`No definitions found! If you are certain definitions should exist - add it manually ))!`);
          } else {
            let none = 'None of the above. I\'ll add it manually!'
            let definitions: string[] = [none];
            definitions.push(... res.data
              .map((data: any) => data.meanings).flat()
              .map((meaning: any) => meaning.definitions).flat()
              .map((obj: any) => obj.definition));
            let def = await inquirer.prompt([{
              type: 'list',
              name: 'choice',
              message: 'Found following Definitions:',
              choices: definitions
            }]);
            if(def.choice !== none) {
              console.log(`=> Adding '${word}' and '${def.choice}'`);
            }
          }
        } catch(err) { }
      }
      state[pointer]++;
    }
    
    if(state[pointer] === 26) {
      state[pointer] = 0;
      pointer--;
      state[pointer]++;
    }
  }
  await fs.writeFile('res/crawler.json', JSON.stringify(state));
}

start();
