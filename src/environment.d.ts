declare global {
  namespace NodeJS {
    interface ProcessEnv {
      ENVIRONMENT?: string;

      CLIENT_ID: string;
      DEV_GUILD?: string;
      TOKEN: string;
    }
  }
}

export {};
