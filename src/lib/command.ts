import { Interaction } from 'discord.js';

export interface CommandExecutable {
  execute(interaction: Interaction): Promise<void>;
}
