class TextBuilder {
  text: string = '';

  addField(key: any, value?: any): TextBuilder {
    if(value) this.text += `**${key}:** \`${value}\`\n`;
    else this.text += `**${key}:**\n`;
    return this;
  }

  addItemField(key: any, value: any): TextBuilder {
    this.text += `❧ **${key}:** \`${value}\`\n`;
    return this;
  }

  print(text: string): TextBuilder {
    this.text += text;
    return this;
  }

  println(text: string): TextBuilder {
    return this.print(text).newline();
  }

  newline(): TextBuilder {
    this.text += '\n';
    return this;
  }

  build(): string {
    return this.text;
  }
}

export default TextBuilder;
