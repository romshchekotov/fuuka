import { REST } from '@discordjs/rest';
import { Routes } from 'discord-api-types/v9';
import { Client, DMChannel, Interaction, Message, TextChannel } from 'discord.js';

import { PrismaClient } from '@prisma/client';

import * as dotenv from 'dotenv';
import { Patterns, replaceNotMatching } from 'regexp-i18n';

import * as fs from 'fs/promises';
import { interactions } from '@/model/interaction';
import { Database, Config } from '@/service';
import {getGame, guess} from './model/interaction/command/play.interaction';

let client: Client;
let prisma: PrismaClient;

/*
 * Configuration & Environment Setup
 */
async function init() {
  const env = process.env.ENVIRONMENT ?? 'develop';
  dotenv.config({ path: `.env.${env}` });
  checkEnvironment();

  prisma = new PrismaClient({ log: ['query'] });
  await upsertInteractions(env);
}

/*
 * Checks whether all required Environment
 * Variables are present.
 */
function checkEnvironment() {
  const required: string[] = ['TOKEN', 'CLIENT_ID'];
  const validEnv: boolean = required.every((v: string) => {
    return !!process.env[v];
  });

  if (!validEnv) {
    throw (
      'Environment not set up properly! Please check the README for ' +
      'Setup Instructions.'
    );
  }
}

/*
 * Send Interactions to the Discord API
 */
async function upsertInteractions(env: string) {
  if (!process.env.TOKEN || !process.env.CLIENT_ID)
    throw 'Invalid Environment!';

  const payload = JSON.parse(await fs.readFile('res/commands.json', 'utf-8'));
  const rest = new REST({ version: '9' }).setToken(process.env.TOKEN);
  console.log('... Initiated refreshing application (/) commands.');
  if (env === 'develop') {
    if (!process.env.DEV_GUILD) {
      throw "Development Environments have to supply a 'DEV_GUILD'.";
    } else {
      try {
        await rest.put(
          Routes.applicationGuildCommands(
            process.env.CLIENT_ID,
            process.env.DEV_GUILD
          ),
          { body: payload }
        );
      } catch (err) {
        console.error(err);
      }
    }
  } else {
    try {
      await rest.put(Routes.applicationCommands(process.env.CLIENT_ID), {
        body: payload,
      });
    } catch (err) {
      console.error(err);
    }
  }
  console.log('Successfully reloaded application (/) commands.');
}

function setup() {
  client = new Client({ intents: Config.intents });

  client.on('ready', () => {
    console.log(`Logged in as ${client.user!.tag}!`);
  });

  client.on('messageCreate', async (message: Message) => {
    let channel = <TextChannel | DMChannel> message.channel;
    
    let game = getGame(message.author.id);
    if(game && message.content.length === game.word.length) {
      let attempt = guess(message.author.id, message.content);
      if(attempt.success) {
        await message.reply({
          content: `Congratulations! You guessed the word (${message.content}) ` +
            `with ${attempt.guesses} guesses remaining!`
        });
      } else if(attempt.guesses > 0) {
        await message.reply({ content: attempt.pattern });
      } else {
        await message.reply({
          content: `Failed to guess the word! The word was: '${game.word}'`
        });
      }
      return;
    }
  
    if(channel instanceof TextChannel && channel.name.includes("english")) {
      let text: string[] = replaceNotMatching(Patterns.MATCH_LETTER, ' ', message.content)
        .split(' ').filter(w => w.length >= 5).map(w => w.toLowerCase());
      for(const word of text) {
        await Database.registerEnglishWord(word, message.author.id);
      }
    }
  });

  client.on('interactionCreate', async (interaction: Interaction) => {
    if (interaction.isCommand()) {
      let found: any,
        subcmd = interaction.options.getSubcommand(false),
        subgroup = interaction.options.getSubcommandGroup(false);

      found = interactions[interaction.commandName];
      if (subgroup) found = found[subgroup];
      if (subcmd) found = found[subcmd];

      if (found) {
        let x = true;
        if(x) { // TODO
          await found.execute(interaction);
        } else {
          return await interaction.reply({
            content: 'Insufficient Permissions. Cannot execute Command.',
            ephemeral: true,
          });
        }
      } else {
        console.error(
          `Non-implemented interaction Received!\n` +
            `Name: ${interaction.commandName}, ` +
            `Subgroup: ${interaction.options.getSubcommandGroup(false)}, ` +
            `Sub Command: ${interaction.options.getSubcommand(false)};`
        );
      }
    }
  });

  client.login(process.env.TOKEN).then(() => {});
}

init().then(setup);

export { client, prisma };
