import { GuildMember, User } from 'discord.js';
import { Config } from '@/service';
import { APIInteractionGuildMember, APIUser } from 'discord-api-types/v9';

export function isUser(
  user: User | APIUser | GuildMember | APIInteractionGuildMember
): user is User | APIUser {
  return (
    (user as GuildMember | APIInteractionGuildMember).permissions === undefined
  );
}

export function isPrivileged(
  user: User | APIUser | GuildMember | APIInteractionGuildMember
): boolean {
  if(isUser(user)) {
    return Config.developers.includes(user.id);
  } else {
    return Config.developers.includes(user.user.id);
  }
}
