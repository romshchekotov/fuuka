import { prisma } from '@/app';
import { CommandExecutable } from '@/lib';
import { CommandInteraction, MessageEmbed } from 'discord.js'

class StatusCommand implements CommandExecutable {
  async execute(interaction: CommandInteraction) {
    const length: number | null = interaction.options.getInteger('length', false);
    const lang: string | null = interaction.options.getString('language', false);
    const word: string | null = interaction.options.getString('word', false);

    let result: any[] = [];
    if(length !== null) {
      try {
        result = await prisma.word.findMany({where: {
          length: {equals: length},
          lang: {equals: lang ?? 'en'},
          approvedBy: {not: null},
          disapprovalReason: {equals: null}
        }});
      } catch(err) {
        console.error(err);
        return await interaction.reply({
          content: `Error encountered! Please report to the developer!`
        });
      }
    } else if(lang !== null) {
      try {
        result = await prisma.word.findMany({where: {
          lang: {equals: lang},
          approvedBy: {not: null},
          disapprovalReason: {equals: null}
        }});
      } catch(err) {
        console.error(err);
        return await interaction.reply({
          content: `Error encountered! Please report to the developer!`
        });
      }
    } else if(word !== null) {
      try {
        let found = await prisma.word.findUnique({ where: { word }, include: { language: true } });
        if(found) {
          let embed: MessageEmbed = new MessageEmbed();
          embed.setTitle(`Word: '${word}'`)
          if(found.disapprovalReason) {
            embed.setColor('DARK_RED')
              .addField('Suggested By', `<@!${found.createdBy}>`)
              .addField('Suggested At', found.createdAt.toISOString())
              .addField('Disapproved By', `<@!${found.approvedBy}>`)
              .addField('Disapproved At', found.approvedAt!.toISOString())
              .addField('Reason', found.disapprovalReason)
          } else if(!found.approvedBy) {
            embed.setColor('YELLOW')
              .addField('Suggested By', `<@!${found.createdBy}>`)
              .addField('Suggested At', found.createdAt.toISOString())
              .addField('Language', found.language.name);
          } else {
            embed.setColor('GREEN')
              .setURL(found.reference!)
              .addField('Definition', found.meaning!)
              .addField('Suggested By', `<@!${found.createdBy}>`)
              .addField('Suggested At', found.createdAt.toISOString())
              .addField('Approved By', `<@!${found.approvedBy}>`)
              .addField('Approved At', found.approvedAt!.toISOString())
              .addField('Language', found.language.name);
          }
          return await interaction.reply({
            embeds: [embed]
          });
        }
        return await interaction.reply({
          content: `The word '${word}' has not been found! Feel free to suggest it.`
        });
      } catch(err) {
        console.error(err);
        return await interaction.reply({
          content: `Error encountered! Please report to the developer!`
        });
      }
    } else {
      return await interaction.reply({ content: '*No Output*\nNo Filters have been chosen.' });
    }

    return await interaction.reply({
      content: `There are ${result.length} items matching your filter!`
    });
  }
}

const instance = new StatusCommand();
export default instance;
