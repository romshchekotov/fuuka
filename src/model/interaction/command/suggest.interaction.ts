import {CommandExecutable} from "@/lib";
import {Database} from "@/service";
import ISO6391 from 'iso-639-1';
import {CommandInteraction} from "discord.js";
import {INVALID_WORD, PRISMA_CON_ERROR, SUCCESS, WORD_EXISTS, WORD_BANNED} from "@/service/db.service";

class SuggestCommand implements CommandExecutable {
  async execute(interaction: CommandInteraction) {
    const word = interaction.options.getString('word', true).toLowerCase();
    let lang = interaction.options.getString('lang', false);
    const author = interaction.user.id;

    let success: number;
    if(!lang) {
      success = await Database.registerEnglishWord(word, author);
      if(success === SUCCESS) {
        return await interaction.reply({
          content: 'Successfully suggested `' + 
            word + '`. Please wait for the staff to review!'
        });
      }
    } else {
      lang = lang.toLowerCase();
      success = await Database.registerWord(word, lang, author);
      if(success === SUCCESS) {
        return await interaction.reply({
          content: `Successfully suggested \`${word}\`` +
            `(${ISO6391.getName(lang) ?? 'Unknown'}). ` +
            `Please wait for the staff to review!`
        });
      }
    }
    if(success === PRISMA_CON_ERROR) {
      return await interaction.reply({
        content: 'An error seems to have occured! Please report this to the developers!'
      });
    } else if(success === INVALID_WORD) {
      return await interaction.reply({
        content: 'Your suggestion does not look like a word. ' + 
          'If you think this is a bug, please report it to the developers!'
      });
    } else if(success === WORD_BANNED) {
      return await interaction.reply({
        content: 'The word you suggest has already been disapproved of when it was suggested ' +
          'by another user! This word will not be added. Thank you for your understanding.'
      });
    } else if(success === WORD_EXISTS) {
      return await interaction.reply({
        content: 'Your suggestion is already registered. Please be patient. ' +
          'If it is not added yet, it will be reviewed sooner or later by the staff team!'
      });
    } else {
      return await interaction.reply({
        content: 'Something went horribly wrong. Please report this incident to the dev team!'
      });
    }
  }
}

const instance = new SuggestCommand();
export default instance;
