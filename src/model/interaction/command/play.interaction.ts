import { Word } from "@prisma/client";
import {CommandExecutable} from "@/lib";
import {Database} from '@/service';
import {CommandInteraction} from "discord.js";

interface GameData {
  user: string;
  word: string;
  guesses: number;
}

let games: GameData[] = [];

class PlayCommand implements CommandExecutable {
  async execute(interaction: CommandInteraction) {

    let game = getGame(interaction.user.id);
    if(game) {
      await interaction.reply({
        content: 'Are you sure you want to reset your current game?\n' +
          '(Reply [yes/no] within 60 seconds, Default: no)',
        ephemeral: true
      });
      if(interaction.channel !== null) {
        let msgs = await interaction.channel.awaitMessages({
          filter: (msg) => msg.author.id === interaction.user.id,
          time: 1000 * 60,
          max: 1
        });
        let msg = msgs.first();
        if(msg) {
          if(/yes/ig.test(msg.content)) {
            await init(interaction);
          }
          await msg.delete();
        }
        return;
      } else {
        console.error(`Fatal Error! Interaction Channel is null!`);
        await interaction.followUp({
          content: 'Something went horribly wrong! Please notify the Developers!'
        });
      }
    } else {
      await interaction.reply({
        content: 'Generating your game!',
        ephemeral: true
      });
      await init(interaction);
    }
  }
  
}

interface GuessFeedback {
  success: boolean;
  pattern: string | null;
  guesses: number;
}

async function init(interaction: CommandInteraction) {
  const length: number = interaction.options.getInteger('length', true);
  const lang = interaction.options.getString('lang', false);

  let word = await randomWord(length, lang);
  if(word) {
    games.push({
      user: interaction.user.id,
      word, guesses: 5
    });
    return await interaction.followUp({
      content: `You (<@!${interaction.user.id}>) can start guessing by typing words of length ${length}.\n` +
        '(one word per message)'
    });
  } 
  return await interaction.followUp({
    content: 'There are no words available for a game given your filter!'
  });
}

async function randomWord(length: number, lang: string | null) {
  let words: Word[];
  if(lang) {
    words = await Database.getWords(lang, length);
  } else {
    words = await Database.getEnglishWords(length);
  }

  if(words.length === 0) {
    return null;
  } 
  return words[Math.floor(Math.random() * words.length)].word;
}

function guess(user: string, guess: string): GuessFeedback {
  let g = getGame(user);
  if(g) {
    let sol = g.word;
    if(sol.length !== guess.length) {
      return { success: false, pattern: null, guesses: g.guesses };
    }

    g.guesses--;
    if(sol === guess || g.guesses === 0) {
      games = games.filter(game => game.user !== user);
    } 

    let pattern = guess.split('').map((v, i) => guessLetter(sol, v, i)).join('');
    return { success: sol === guess, pattern, guesses: g.guesses };
  } 
  return { success: false, pattern: null, guesses: -1 };
}

function guessLetter(solution: string, letter: string, index: number) {
  if(solution.charAt(index) === letter) {
    return '🟩';
  } else if(solution.includes(letter)) {
    return '🟨';
  }
  return '🟥';
}

function getGame(user: string): GameData | undefined {
  return games.find(game => game.user === user);
}

const instance = new PlayCommand();
export default instance;
export { getGame, guess };
