import {prisma} from "@/app";
import {CommandExecutable} from "@/lib";
import {Config} from "@/service";
import {CommandInteraction} from "discord.js";

class SuggestionsCommand implements CommandExecutable {
  async execute(interaction: CommandInteraction) {
    if(!Config.developers.includes(interaction.user.id)) {
      return await interaction.reply({
        content: 'Insufficient Permissions to list suggestions!',
        ephemeral: true
      });
    }

    let toApprove = await prisma.word.findMany({
      where: {
        approvedBy: { equals: null }
      },
      select: {
        word: true,
        lang: true
      },
      orderBy: [
        { uses: 'desc' }, 
        { createdAt: 'asc' }
      ]
    });

    if(toApprove.length === 0) {
      return await interaction.reply({
        content: 'Done with the Suggestions List! Nothing left to do! Good job ))',
        ephemeral: true
      });
    }

    toApprove = toApprove.slice(0, Math.max(toApprove.length, 10));
    let message = toApprove.map(entry => `🙙 ${entry.word} (${entry.lang})`).join('\n');
    
    return await interaction.reply({
      content: message,
      ephemeral: true
    });
  }
}

const instance = new SuggestionsCommand();
export default instance;
