import {CommandExecutable} from "@/lib";
import {Config, Database} from "@/service";
import { URL } from "@/util/regex.util";
import {CommandInteraction, MessageEmbed} from "discord.js";

class ApproveCommand implements CommandExecutable {
  async execute(interaction: CommandInteraction) {
    if(!Config.developers.includes(interaction.user.id)) {
      return await interaction.reply({
        content: 'Insufficient Permissions to approve a word!',
        ephemeral: true
      });
    }

    let word = interaction.options.getString('word', true);
    let def = interaction.options.getString('definition', true);
    let ref = interaction.options.getString('reference', true);
    let lang = interaction.options.getString('language', false) ?? 'en';

    if(!URL.test(ref)) {
      return await interaction.reply({
        content: 'Invalid URL!',
        ephemeral: true
      });
    }

    let approval = await Database.approveWord(
      word, def, ref, interaction.user.id, lang ?? undefined);
    let embed: MessageEmbed = new MessageEmbed()
      .setTitle(`Word: ${word}`)
      .setURL(ref)
      .setAuthor({ 
        name: interaction.user.tag, 
        iconURL: interaction.user.avatarURL({ size: 512, dynamic: true }) ??
          interaction.user.defaultAvatarURL
      }).setDescription(`**Definition:** ${def}`);
    if(approval) {
      embed.setColor("GREEN").addField('Approved', '✅');
    } else {
      embed.setColor("DARK_RED").addField('Approved', '❌ (technical)');
    }

    return await interaction.reply({
      embeds: [embed]
    });
  }
}

const instance = new ApproveCommand();
export default instance;
