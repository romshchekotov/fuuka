import {CommandExecutable} from "@/lib";
import { Config, Database } from "@/service";
import {CommandInteraction} from "discord.js";

class DisapproveCommand implements CommandExecutable {
  async execute(interaction: CommandInteraction) {
    if(!Config.developers.includes(interaction.user.id)) {
      return await interaction.reply({
        content: 'Insufficient Permissions to list suggestions!',
        ephemeral: true
      });
    }
   
    let word = interaction.options.getString('word', true);
    let reason = interaction.options.getString('reason', true);

    let disapproved = await Database.disapproveWord(word, interaction.user.id, reason);

    if(disapproved) {
      return await interaction.reply({
        content: `Successfully disapproved the word '${word}'.`,
        ephemeral: true
      });
    }

    return await interaction.reply({
      content: 'Failed to disapprove word! Please ask the developer to fix it!',
      ephemeral: true
    });
  }
}

const instance = new DisapproveCommand();
export default instance;
