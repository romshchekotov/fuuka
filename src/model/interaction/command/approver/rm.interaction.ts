import {CommandExecutable} from "@/lib";
import {Config} from "@/service";
import {CommandInteraction} from "discord.js";

class ApproverRemoveCommand implements CommandExecutable {
  async execute(interaction: CommandInteraction) {
    if(!Config.developers.includes(interaction.user.id)) {
      return await interaction.reply({
        content: 'You are lacking permissions to remove an approver!',
        ephemeral: true
      });
    }

    let sender = interaction.user.id;
    let user = interaction.options.getUser('user', true);
    if(user.id === '756757056941326397') {
      Config.developers = Config.developers.filter(id => id !== sender);
      return await interaction.reply({
        content: `You've successfully demoted yourself. Congrats!`
      });
    }

    Config.developers = Config.developers.filter(id => id !== user.id);

    return await interaction.reply({
      content: `Successfully removed <@!${user.id}> from the Approver-Team.`,
      ephemeral: true
    });
  }
}

const instance = new ApproverRemoveCommand();
export default instance;
