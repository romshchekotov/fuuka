import {CommandExecutable} from "@/lib";
import {Config} from "@/service";
import {CommandInteraction} from "discord.js";

class ApproverAddCommand implements CommandExecutable {
  async execute(interaction: CommandInteraction) {
    if(!Config.developers.includes(interaction.user.id)) {
      return await interaction.reply({
        content: 'You are lacking permissions to add an approver!',
        ephemeral: true
      });
    }

    let user = interaction.options.getUser('user', true);
    Config.developers.push(user.id);

    return await interaction.reply({
      content: `Successfully added <@!${user.id}> to the Approver-Team.`
    });
  }
}

const instance = new ApproverAddCommand();
export default instance;
