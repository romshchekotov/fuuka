import ApproverAddCommand from './command/approver/add.interaction';
import ApproverRemoveCommand from './command/approver/rm.interaction';
import ApproveCommand from './command/approve.interaction';
import SuggestCommand from './command/suggest.interaction';
import PlayCommand from './command/play.interaction';
import StatusCommand from './command/status.interaction';
import SuggestionsCommand from './command/suggestions.interaction';
import DisapproveCommand from './command/disapprove.interaction';

const interactions = {
  approver: {
    add: ApproverAddCommand,
    rm: ApproverRemoveCommand
  },
  approve: ApproveCommand,
  suggest: SuggestCommand,
  play: PlayCommand,
  status: StatusCommand,
  suggestions: SuggestionsCommand,
  disapprove: DisapproveCommand
};

export { interactions };
