import * as fs from 'fs';

describe('checks command -> file correspondance', () => {
  const jsonFile = fs.readFileSync('res/commands.json', 'utf-8');
  const json: any[] = JSON.parse(jsonFile);
  
  json.forEach((cg) => {
    if(cg.type && cg.type === 1) {
      test(`${cg.name} has a command file.`, () => {
        const path = `src/model/interaction/command/${cg.name}.interaction.ts`;
        const exists = fs.existsSync(path);
        expect(exists).toBeTruthy();
      });
    } else if(cg.options) {
      cg.options.forEach((c: any) => {
        if(c.type && c.type === 1) {
          test(`${cg.name}/${c.name} has a command file.`, () => {
            const path = `src/model/interaction/command/${cg.name}/${c.name}.interaction.ts`;
            const exists = fs.existsSync(path);
            expect(exists).toBeTruthy();
          });
        }
      });
    }
  });
});
